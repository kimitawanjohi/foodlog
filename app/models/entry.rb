class Entry < ApplicationRecord

  validates :calories, :carbohydrates, :proteins, :fats, presence: true

 def day
   self.created_at.strftime("%B %e, %Y")
 end
end
